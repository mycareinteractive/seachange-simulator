<?php 
error_reporting(E_ALL);
ini_set('display_errors', 'On');

// header("Content-type: text/plain");
header("Content-type: application/json");

$file_path = "./stbservlet_data/";
$file_name = "error.json";

class Params {
	const app_uid = "application_uid";
    const attribute = "attribute";
    const deviceid = "device_id";
    const entry_type = "entry_type";
    const hierarchyuid = "hierarchy_UID";
    const homeid = "home_id";
    const localentryuid = "local_entry_uid";
    const pageno = "page_no";
    const pagesize = "page_size";
    const parenthuid = "parent_HUID";
    const productofferingid = "product_offering_id";
    const regionid = "region_id";
	const timeid = "time";
	const depth = "depth";
	const language = "language";
}

$action = $_GET[Params::attribute];
if($action == NULL) {
	return;
}
$rawoutput = false;
switch($action) {
    case "json_libs_clu_get_region_channel_group":
		$file_name = "regiongroup.json";
		break;
    case "json_libs_stb_get_asset":
		$entryuid = $_GET[Params::localentryuid];
		$file_name = "getasset_".$entryuid.".json";
		break;
    case "json_libs_oss_check_home_status":
		$file_name = "homestatus.json";
		break;
    case "json_libs_oss_account_review_for_one_guest":
    	$file_name = "accountreview.json";
		break;
    case "json_libs_oss_check_service_entitlement":
		$poid = $_GET[Params::productofferingid];
    	$file_name = "checkservice_".$poid.".json";
		break;
    case "json_libs_oss_get_user_data":
		$file_name = "userdata.json";
		break;
    case "json_libs_oss_list_guest_data":
		$file_name = "guestdata.json";
		break;
    case "json_libs_ote_get_product_offering":
		$poid = $_GET[Params::productofferingid];
    	$file_name = "getproduct_".$poid.".json";
		break;
    case "json_libs_ote_get_server_load_info":
		$file_name = "serverload.json";
		break;
    case "json_libs_ote_list_ticket":
		$file_name = "listticket.json";
		break;
	case "json_libs_ote_get_ticket":
		$file_name = "getticket.json";
		break;
    case "json_libs_ote_purchase_product":
		//$entryuid = $_GET[Params::localentryuid];
    	$file_name = "purchaseproduct.json";
		break;
    case "json_libs_stb_list_entry":
		$huid = $_GET[Params::parenthuid];
		$depth = "";
    	$pageparam = "";
    	if(array_key_exists(Params::depth, $_GET) && $_GET[Params::depth]!="" && $_GET[Params::depth]!="1") {
    		$depth = "_all";
    	}
		if(array_key_exists(Params::pageno, $_GET) && $_GET[Params::pageno]!="" && $_GET[Params::pageno]!="1") {
    		$pageparam = "_page".$_GET[Params::pageno];
    	}
    	$file_name = "listentry_".$huid.$pageparam.$depth.".json";
		// replace poster URL to use local server ip/host
		$rawoutput = true;
		
        // normalize to *nix-style path using forwardslash for cross-platform compatibility
        $cdir = str_replace("\\", "/", dirname(__FILE__)); 
        // build the full requested data path. full/absolute system path prevents file_get_contents from failing on particular platforms.
        $dpath = $cdir . "/stbservlet_data/" . $file_name;
        $content = file_get_contents($dpath);
		
        // if using iis, we need to check for the server IP address 
        // differnt than other webservers.
        $server = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') 
            ? $_SERVER['LOCAL_ADDR'] 
            : $_SERVER['SERVER_ADDR'];

        $content = preg_replace("/(http:\/\/)(.+)(\/Poster)/i", "\${1}$server\${3}", $content);
    	break;
    case "json_test_epg":
    	$timeid = $_GET[Params::timeid];
	    $timestr = substr($timeid,8,4);
		$dateid = substr($timeid,0,4)."-".substr($timeid,4,2)."-".substr($timeid,6,2);
	    $today = date("Y-m-d");
		$tomorrow = date("Y-m-d",strtotime("+1 day"));
		$file_name = "testepg_".$timestr.".txt";
		$rawoutput = true;
        
        // normalize to *nix-style path using forwardslash for cross-platform compatibility
        $cdir = str_replace("\\", "/", dirname(__FILE__)); 
        // build the full requested data path. full/absolute system path prevents file_get_contents from failing on particular platforms.
        $dpath = $cdir . "/stbservlet_data/" . $file_name;
        $content = file_get_contents($dpath);
        
		$content = str_replace("2010-10-06", $dateid, $content);
		$content = str_replace("2010-10-07", $tomorrow, $content);
		break;
	case "ewf_get_user_data":
		$file_name = "ewf_userdata.json";
		break;
	case "json_libs_oss_stb_info_setting":
		break;
	case "json_libs_oss_set_language":
		$lang = $_GET[Params::language];
		$str=file_get_contents($file_path.'userdata.json');
		$str=str_replace("\"en\"", "\"".$lang."\"",$str);
		file_put_contents($file_path.'userdata.json', $str);
		$rawoutput = true;
		$content = '{ "DataArea": {"tagName": "Result","tagAttribute": { "result": "True"} } }';
		break;
	case "json_libs_oss_set_blockpin":
		break;
    case "json_libs_oss_list_service_package":
        $file_name = "listservicepackage.json";
		break;
        break;
};

if(!$rawoutput) {
    // normalize to *nix-style path using forwardslash for cross-platform compatibility
    $cdir = str_replace("\\", "/", dirname(__FILE__)); 
    // build the full requested data path. full/absolute system path prevents file_get_contents from failing on particular platforms.
    $dpath = $cdir . "/stbservlet_data/" . $file_name;
	$content = file_get_contents($dpath);
    // echo $content;
}


echo $content;

?>