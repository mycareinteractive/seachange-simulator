<?php
 
$header[] = "Content-type: text/xml";

$file_path = "./middleware_data/";
$file_name = "error.json";

$method = $_SERVER['REQUEST_METHOD'];
$url = $_SERVER['QUERY_STRING'];
$pos = strpos($url, 'wsdl=');
$url = substr($url, $pos + 5);


if(strpos($url, 'localhost:8025/getPatron')) {
	$file_name = "getPatron.xml";
	$content = file_get_contents($file_path.$file_name);
	print $content;
	return;
}

if(strpos($url, 'localhost:8026/getPatronMenu')) {
	$file_name = "getPatronMenu.xml";
	$content = file_get_contents($file_path.$file_name);
	print $content;
	return;
}

if(strpos($url, 'localhost:8027/getGuestMenu')) {
	$file_name = "getPatronMenu.xml";
	$content = file_get_contents($file_path.$file_name);
	print $content;
	return;
}

if(strpos($url, 'localhost:8028/submitPatronMenu')) {
	$file_name = "getPatronMenu.xml";
	$content = file_get_contents($file_path.$file_name);
	print $content;
	return;
}

if(strpos($url, 'localhost:8029/submitGuestMenu')) {
	$file_name = "getPatronMenu.xml";
	$content = file_get_contents($file_path.$file_name);
	print $content;
	return;
}

if(strpos($url, 'localhost:8030/unlockPatronMenu')) {
	$file_name = "getPatronMenu.xml";
	$content = file_get_contents($file_path.$file_name);
	print $content;
	return;
}




else if(strpos($url, 'localhost:9080/ams/aceso/getMenuXml')) {
	$file_name = "getMenuXml.xml";
	$dir = str_replace("\\", "/", dirname(__FILE__) . "/");
	$content = file_get_contents($dir.$file_path.$file_name);
	print $content;
	return;
}
else if(strpos($url, 'localhost:9080/ams/aceso/getAgendaJson')) {
	$file_name = "getAgendaJson.json";
	$dir = str_replace("\\", "/", dirname(__FILE__) . "/");
	$content = file_get_contents($dir.$file_path.$file_name);
	print $content;
	return;
}
else if(strpos($url, 'localhost:9080/ams/aceso/getClinicalData')) {
    $query = parse_url($url, PHP_URL_QUERY);
    parse_str($query, $params);
    $file_name = "getClinicalData_" . $params['type'] . '.xml';
	$dir = str_replace("\\", "/", dirname(__FILE__) . "/");
	$content = file_get_contents($dir.$file_path.$file_name);
	print $content;
	return;
}
else if(strpos($url, 'GetMeals')) {
	$today = date("Y-m-d");
	$tomorrow = date("Y-m-d",strtotime("+1 day"));
	$file_name = "GetMeals.xml";
	$dir = str_replace("\\", "/", dirname(__FILE__) . "/");
	$content = file_get_contents($dir.$file_path.$file_name);
	$content = str_replace("2014-04-10", $today, $content);
	$content = str_replace("2014-04-11", $tomorrow, $content);
	print $content;
	return;
}
else if(strpos($url, 'GetFood')) {
	$today = date("Y-m-d");
	$tomorrow = date("Y-m-d",strtotime("+1 day"));
	$file_name = "GetFood.xml";
	$dir = str_replace("\\", "/", dirname(__FILE__) . "/");
	$content = file_get_contents($dir.$file_path.$file_name);
	$content = str_replace("2014-04-10", $today, $content);
	$content = str_replace("2014-04-11", $tomorrow, $content);
	print $content;
	return;
}
else if(strpos($url, 'SubmitOrder')) {
	$file_name = "SubmitOrder.xml";
	$dir = str_replace("\\", "/", dirname(__FILE__) . "/");
	$content = file_get_contents($dir.$file_path.$file_name);
	print $content;
	return;
}
$ch = curl_init($url); 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT_MS, 500);
if($method == "POST") {
	$postvars = '';
	while ($element = current($_POST)) {
		if(strlen($postvars)>0) $postvars .= '&';
		$postvars .= $element;
		next($_POST);
	}
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
}

$response = curl_exec($ch);     
$response_headers = curl_getinfo($ch);     

if (curl_errno($ch)) {
    print curl_error($ch);
} else {
    curl_close($ch);
	if(isset($response_headers['content-type']))
		header( 'Content-type: ' . $response_headers['content-type']);
	else 
		header( 'Content-type: text/xml');
    print $response;
}


?>