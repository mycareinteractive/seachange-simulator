How to Setup an Aceso VOD simulation server


---------------------------------------------------------
Install VC10 Binary
---------------------------------------------------------
You must install Visual C++ 2010 SP1 Redistributable Package x86 or x64
VC10 SP1 vcredist_x86.exe 32 bits: http://www.microsoft.com/download/en/details.aspx?id=8328

VC10 SP1 vcredist_x64.exe 64 bits: http://www.microsoft.com/download/en/details.aspx?id=13523


---------------------------------------------------------
Install WAMP
---------------------------------------------------------
WAMP is a windows bundle for Apache, PHP and MySQL.  Choose 64 bit or 32 bit version according to your Windows version, double click the install file to install wamp at default location C:\wamp
Configure WAMP to allow inbound connection
Launch WAMP from desktop or start menu.  Left click the WAMP icon in the tray, ��Apache��->��httpd.conf��.
1.In the configuration file search for <Directory "c:/wamp/www/">, before the closing tab </Directory>, you will see the Deny from all.  Change it to Allow from all.
��.
Order Deny,Allow
Allow from all

2.Uncommented rewrite module by remove the ��#�� before LoadModule rewrite_module modules/mod_rewrite.
#LoadModule rewrite_module modules/mod_rewrite.so
TO
LoadModule rewrite_module modules/mod_rewrite.so


---------------------------------------------------------
Copy simulator folder to WAMP
---------------------------------------------------------
Unzip www.zip file to C:\wamp\www, so that you will see folder structure such as:
C:\wamp\www\ewf_fahc
C:\wamp\www\Poster
��

---------------------------------------------------------
Copy sample folder to local folder to allow local manipulate
---------------------------------------------------------
Copy middleware_data.sample folder to middleware_data folder
Copy stbservlet_data.sample folder to stbservlet_data folder


---------------------------------------------------------
Restart WAMP
---------------------------------------------------------
Restart WAMP by left click WAMP icon in tray, ��Restart All Services��.


---------------------------------------------------------
Configure Window Firewall
---------------------------------------------------------
Go to window firewall, click ��Advanced settings�� on the left panel, a new window ��Windows Firewall with Advanced Security�� will be opened.
Click ��Inbound Rules�� on the left tree, and then click ��New Rule���� on the right panel.
Allow the following ports
TCP	80	Allow
RTSP	554	Allow


---------------------------------------------------------
Configure Enseo STB startup URL
---------------------------------------------------------
Option 1
If you have a router that��s running dd-wrt firmware, you can add boot url into the router by click ��Services��.  Under ��DNSMasq�� enable DNSMasq, then put the following lines in ��Additional DNSMasq Options��
dhcp-option=52,1
dhcp-option=67,http://192.168.1.50/startup/
Replace the 192.168.1.50 with your Windows PC/Laptop IP.  It is recommended to make your laptop retaining the same IP between reboots, so that you don��t need to change boot up URL.

Option 2
Load a QA firmware on Enseo STB.  Then connect to console, press ��a��, choose option 60, then type in boot URL.  Eg: http://192.168.1.50/startup/
The ��startup�� folder in www will automatically redirect to ewf_fahc.


---------------------------------------------------------
Launch VOD RTSP server
---------------------------------------------------------
Double click C:\wamp\www\video\live555MediaServer.exe file to start VOD RTSP server.


---------------------------------------------------------
Add video files
---------------------------------------------------------
Put video files in www\video folder, rename it to <asset ID>.ts file.  A sample video 1000000083.ts has been provided for reference, which is mapped to ��Harry Potter�� in the UI.
The file should be mpeg2 or h264 mpeg transport stream file.  The bitrate of the video should comply with your laptop horsepower.  Typically we don��t recommend 1080p Mpeg2 video due to the huge bandwidth requirement.
No need to restart Live555MediaServer.exe after adding video files.


---------------------------------------------------------
Changes to Portal UI
---------------------------------------------------------
Most of the existing Enseo UI should work with the simulator, with a couple changes:
?	Modify CommonJSON.js to set correct parameters.
?	Add a new ��VODServerType�� into CommonJSON settings.  If value equals ��Live555�� it will send the RTSP format to comply with Live555 server.
?	Use the new playvideo.js with support for Live555 RTSP server.


---------------------------------------------------------
Under the hood
---------------------------------------------------------
The ��stbservlet.php�� script maps each JSON request to a static text file in c:\wamp\www\stbservlet_jsol\ folder.
By changing the content in the text file (*.json) you can feed the STB with various data.
