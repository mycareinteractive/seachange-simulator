<?php 
header("Content-type: text/javascript");
//window.serverTime = {"year":2015,"month":11,"day":9,"hour":12,"minute":5,"second":17,"gmtOffsetInMinute":360,"isDaylightSaving":false};

$today = date("Y-m-d");
$content = sprintf("window.serverTime = {\"year\":%s,\"month\":%s,\"day\":%s,\"hour\":%s,\"minute\":%s,\"second\":%s,\"gmtOffsetInMinute\":%s,\"isDaylightSaving\":false};"
    ,date("Y"), date("n"), date("j"), date("G"), date("i"), date("s"), date("Z")/-60, date("I")=="1"?true:false);

echo $content;

?>